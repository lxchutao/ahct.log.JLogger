package abc;

import java.util.Date;

import ahct.log.JLogger;

/**
 * TestLogDemo
 * 
 * @author ChuTao
 *
 */
public class TestLogDemo
{
	static JLogger logger = JLogger.getInstance();

	public static void TestDemo1()
	{
		logger.Debug("TestDemo1测试TestDemo1测试环境Debug日志dd" + new Date());
		logger.Debug("TestDemo1测试环境打印出的调试日志" + new Date());
		logger.Info("TestDemo1测试环境打印出的信息日志" + new Date());
		logger.Warn("TestDemo1测试环境打印出的警告日志" + new Date());
		logger.Error("TestDemo1测试环境打印出的一般错误日志" + new Date());
		logger.Fatal("TestDemo1测试环境打印出的致命错误日志" + new Date());
	}

	public static void TestDemo2()
	{
		JLogger.getInstance().Debug("测试环境打印出的Debug日志");
		JLogger.getInstance().Info("测试环境打印出的Info日志");
		JLogger.getInstance().Warn("测试环境打印出的Warn日志");
		JLogger.getInstance().Error("测试环境打印出的Error日志");
		JLogger.getInstance().Fatal("测试环境打印出的Fatal日志");
	}

}
