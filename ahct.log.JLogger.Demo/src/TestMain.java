import java.text.SimpleDateFormat;
import java.util.Date;

//import ahct.log.LogLevel;
import ahct.log.JLogger;

import aac.TestJLogDemo;
import abc.TestLogDemo;
import abc.efg.TestLogDemo1;


/**
 * TestMain
 * @author ChuTao
 *
 */
public class TestMain
{
    /**
     * <一句话功能简述> <功能详细描述>
     * 
     * @param args
     * @throws InterruptedException 
     * @see [类、类#方法、类#成员]
     */
    public static void main(String[] args) throws InterruptedException
    {
        System.out.println(System.getenv("temp")); // C:\Users\ADMINI~1\AppData\Local\Temp
        System.out.println(System.getenv("TEMP")); // C:\Users\ADMINI~1\AppData\Local\Temp
        System.out.println(System.getenv("JAVA_HOME")); // C:\Program Files (x86)\Java\jdk1.6.0_10
        
        JLogger logger = JLogger.getInstance();
        
        //LogLevel.DEBUG ;//OK
        boolean bln = true;
//        while(bln)
//        {
	        for (int i = 0; i < 10000; i++)
	        {
	            logger.Debug("测试Debug日志dd" + new SimpleDateFormat("yyyy-MM-dd hh:mm:ss,SSS").format(new Date()));
	            logger.Debug("环境打印出的调试日志" + new SimpleDateFormat("yyyy-MM-dd hh:mm:ss,SSS").format(new Date()));
	            logger.Info("环境打印出的信息日志" + new SimpleDateFormat("yyyy-MM-dd hh:mm:ss,SSS").format(new Date()));
	            logger.Warn("环境打印出的警告日志" + new SimpleDateFormat("yyyy-MM-dd hh:mm:ss,SSS").format(new Date()));
	            logger.Error("环境打印出的一般错误日志" + new SimpleDateFormat("yyyy-MM-dd hh:mm:ss,SSS").format(new Date()));
	            logger.Fatal("环境打印出的致命错误日志" + new SimpleDateFormat("yyyy-MM-dd hh:mm:ss,SSS").format(new Date()));
	            
	            TestLogDemo.TestDemo1();
	            TestLogDemo1.TestDemo1();
	            TestLogDemo1.TestDemo2();
	            TestLogDemo.TestDemo2();
	            TestJLogDemo.TestDemo1();
	            TestJLogDemo.TestDemo2();
	        }
//	        Thread.sleep(60000);
//        }
        System.out.println("Finished.________________________________________________Finished.");
    }
}
