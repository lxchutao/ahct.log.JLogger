package com.ahct.logdemo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import ahct.log.JLogger;

import com.ahct.logdemo.model.User;

/**
 * @author ChuTao
 *
 */
@Controller
public class UserController 
{
	@RequestMapping("")
	public String Portal(Model model) 
	{
		JLogger.getInstance().debug("Portal model:" + model);
		JLogger.getInstance().info("Portal model:" + model);
		JLogger.getInstance().warn("Portal model:" + model);
		JLogger.getInstance().error("Portal model:" + model);
		JLogger.getInstance().fatal("Portal model:" + model);
		return "portal";
	}
	
	@RequestMapping("/save")
	public String Save(@ModelAttribute("form") User user, Model model) 
	{ 
		JLogger.getInstance().debug("Save user:" + user);
		JLogger.getInstance().info("Save model:" + model);
		JLogger.getInstance().warn("Save user:" + user);
		JLogger.getInstance().error("Save model:" + model);
		JLogger.getInstance().fatal("Save user:" + user);
		
		model.addAttribute("user", user);
		return "result";
	}
}
