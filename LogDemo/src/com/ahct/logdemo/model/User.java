package com.ahct.logdemo.model;

import java.util.Date;

/**
 * @author ChuTao
 *
 */
public class User 
{
	private int id;
	private String name;
	private String sex;
	private String work;
	private Date createTime = new Date();
	
	public final int getId() {
		return id;
	}
	public final void setId(int id) {
		this.id = id;
	}
	public final String getName() {
		return name;
	}
	public final void setName(String name) {
		this.name = name;
	}
	public final String getSex() {
		return sex;
	}
	public final void setSex(String sex) {
		this.sex = sex;
	}
	public final String getWork() {
		return work;
	}
	public final void setWork(String work) {
		this.work = work;
	}
	public final Date getCreateTime() {
		return createTime;
	}
	public final void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	
	
}
