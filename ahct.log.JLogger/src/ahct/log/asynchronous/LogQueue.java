package ahct.log.asynchronous;

import java.util.concurrent.LinkedBlockingQueue;

/**
 * LogQueue
 * @author ChuTao
 * Copyright © lxchutao(楚涛) 2011-2015  lxchutao@163.com  QQ:290363711  http://blog.csdn.net/chutao  http://lxchutao.cnblogs.com
 */
public class LogQueue
{
    /**
     * 日志 同步阻塞队列
     */
    public static LinkedBlockingQueue<LogEntity> LOG_QUEUE = new LinkedBlockingQueue<LogEntity>();
}
