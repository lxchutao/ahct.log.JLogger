package ahct.log.asynchronous;

import java.io.File;
import java.util.ArrayList;

/**
 * LogWrite
 * @author ChuTao
 * Copyright © lxchutao(楚涛) 2011-2015  lxchutao@163.com  QQ:290363711  http://blog.csdn.net/chutao  http://lxchutao.cnblogs.com
 */
class LogWrite implements Runnable
{
    private static ArrayList<LogWriteThreadByFile> lstLogFileThread = new ArrayList<LogWriteThreadByFile>();
    
    private int _threadWait = 100;
    
    private boolean _threadIsDaemon = false;
    
    public LogWrite(int wait, Boolean threadIsDaemon)
    {
        _threadWait = wait;
        _threadIsDaemon = threadIsDaemon;
    }
    
    @Override
    public void run()
    {
        while (true)
        {
            while (LogQueue.LOG_QUEUE.size() > 0)
            {
                try
                {
                    LogEntity entity = LogQueue.LOG_QUEUE.take();
                    if (entity != null)
                    {
                        String file_dir_name = entity.get_file_dir_name();
                        String logFileSuffix = entity.get_log_file_suffix();
                        String log_extension = entity.get_log_extension();
                        
                        // 日志文件路径+文件名+扩展名
                        String filename_full = file_dir_name + logFileSuffix + log_extension;
                        
                        File file = new File(filename_full);
                        if (!file.getParentFile().exists())
                        {
                            file.getParentFile().mkdirs();
                        }
                        
                        boolean isFind = false;
                        for (LogWriteThreadByFile item : lstLogFileThread)
                        {
                            if (item.get_filename_full().equals(filename_full))
                            {
                                item.get_queue_contents().put(entity);
                                isFind = true;
                            }
                        }
                        
                        if (!isFind)
                        {
                            LogWriteThreadByFile lwt = new LogWriteThreadByFile(filename_full, this._threadWait,
                                this._threadIsDaemon);
                            lwt.get_queue_contents().put(entity);
                            lstLogFileThread.add(lwt);
                        }
                    }
                }
                catch (InterruptedException e)
                {
                    e.printStackTrace();
                }
            }
            if (LogQueue.LOG_QUEUE.size() <= 0)
            {
                if (_threadWait > 0)
                {
                    try
                    {
                        Thread.sleep(_threadWait);
                    }
                    catch (InterruptedException e)
                    {
                        e.printStackTrace();
                    }
                }
            }
        }
    }
}
