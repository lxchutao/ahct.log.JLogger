package ahct.log.asynchronous;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.util.Date;
import java.util.concurrent.LinkedBlockingQueue;

import ahct.log.FileOperate;
import ahct.log.JLogger;

/**
 * LogWriteThreadByFile
 * @author ChuTao
 * Copyright © lxchutao(楚涛) 2011-2015  lxchutao@163.com  QQ:290363711  http://blog.csdn.net/chutao  http://lxchutao.cnblogs.com
 */
class LogWriteThreadByFile
{
    private final String _filename_full;
    
    private final LinkedBlockingQueue<LogEntity> _queue_contents = new LinkedBlockingQueue<LogEntity>();
    
    private int _thread_wait = 100;
    
    public LogWriteThreadByFile(String filename_full, int threadWait, boolean isThreadDaemon)
    {
        this._filename_full = filename_full;
        if (threadWait > 0)
        {
            this._thread_wait = threadWait;
        }
        Thread _thread = new Thread(new WriteLogContents());
        if (isThreadDaemon)
        {
            _thread.setDaemon(true);
        }
        _thread.start();
    }
    
    public final String get_filename_full()
    {
        return _filename_full;
    }
    
    public final LinkedBlockingQueue<LogEntity> get_queue_contents()
    {
        return _queue_contents;
    }
    
    class WriteLogContents extends Thread
    {
        public void run()
        {
            while (true)
            {
                while (get_queue_contents().size() > 0)
                {
                    try
                    {
                        LogEntity entity = get_queue_contents().take();
                        if (entity != null)
                        {
                            try
                            {
                                File file = new File(get_filename_full());
                                if (file.exists())
                                {
                                    if (file.length() > JLogger.getInstance().getLogFileMaxsize())
                                    {
                                        String path = entity.get_file_dir_name() + entity.get_log_file_suffix() + "_"
                                            + new Date().getTime() + entity.get_log_extension();
                                        File destFile = new File(path);
                                        // boolean isRenamed = file.renameTo(destFile);
                                        // boolean isCreated = file.createNewFile();
                                        // System.out.println(isRenamed + "  " + isCreated);
                                        FileOperate.copyUseChannel(file, destFile);
                                        FileOutputStream out = new FileOutputStream(get_filename_full(), false);
                                        out.write(0);
                                    }
                                }
                                else
                                {
                                    file.createNewFile();
                                }
                                
                                FileOutputStream out = new FileOutputStream(get_filename_full(), true);
                                PrintStream p = new PrintStream(out);
                                p.println(entity.get_log_contents());
                                // out.write(contents.getBytes("utf-8"));
                            }
                            catch (FileNotFoundException e)
                            {
                                e.printStackTrace();
                            }
                            catch (IOException e)
                            {
                                e.printStackTrace();
                            }
                        }
                    }
                    catch (InterruptedException e)
                    {
                        e.printStackTrace();
                    }
                }
                if (get_queue_contents().size() <= 0)
                {
                    if (_thread_wait > 0)
                    {
                        try
                        {
                            Thread.sleep(_thread_wait);
                        }
                        catch (InterruptedException e)
                        {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }
    }
}
