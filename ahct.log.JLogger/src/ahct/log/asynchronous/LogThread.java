package ahct.log.asynchronous;

/**
 * LogThread
 * @author ChuTao
 * Copyright © lxchutao(楚涛) 2011-2015  lxchutao@163.com  QQ:290363711  http://blog.csdn.net/chutao  http://lxchutao.cnblogs.com
 */
public class LogThread
{
    /**
     * 启动日志异步写入线程
     * 
     * @see [类、类#方法、类#成员]
     */
    public static void RunAsynchronous(int wait, Boolean threadIsDaemon)
    {
        LogWrite write = new LogWrite(wait, threadIsDaemon);
        Thread logThread = new Thread(write);
        if (threadIsDaemon)
        {
            logThread.setDaemon(true);
        }
        logThread.start();
    }
}
