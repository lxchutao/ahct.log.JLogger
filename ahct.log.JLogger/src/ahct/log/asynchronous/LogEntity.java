package ahct.log.asynchronous;

/**
 * LogEntity
 * @author ChuTao
 * Copyright © lxchutao(楚涛) 2011-2015  lxchutao@163.com  QQ:290363711  http://blog.csdn.net/chutao  http://lxchutao.cnblogs.com
 */
public class LogEntity
{
    /**
     * 日志内容
     */
    private String _log_contents;
    
    /**
     * 日志文件路径+文件名（不含文件扩展名）
     */
    private String _file_dir_name;
    
    /**
     * 日志文件后缀（用以分开记录日志之用，空为不分开记录日志）
     */
    private String _log_file_suffix;
    
    /**
     * 日志文件扩展名
     */
    private String _log_extension;
    
    public final String get_log_contents()
    {
        return _log_contents;
    }
    
    public final void set_log_contents(String _log_contents)
    {
        this._log_contents = _log_contents;
    }
    
    public final String get_file_dir_name()
    {
        return _file_dir_name;
    }
    
    public final void set_file_dir_name(String _file_dir_name)
    {
        this._file_dir_name = _file_dir_name;
    }
    
    public final String get_log_file_suffix()
    {
        return _log_file_suffix;
    }
    
    public final void set_log_file_suffix(String _log_file_suffix)
    {
        this._log_file_suffix = _log_file_suffix;
    }
    
    public final String get_log_extension()
    {
        return _log_extension;
    }
    
    public final void set_log_extension(String _log_extension)
    {
        this._log_extension = _log_extension;
    }
    
}
