package ahct.log;

import java.io.File;
import java.io.IOException;
import java.util.Calendar;
import java.util.Date;

/**
 * 监控日志文件的存储（先压缩再删除超过指定时间的日志文件）（默认30天）
 * @author ChuTao
 * Copyright © lxchutao(楚涛) 2011-2015  lxchutao@163.com  QQ:290363711  http://blog.csdn.net/chutao  http://lxchutao.cnblogs.com
 */
class MonitorLogFileStorage extends Thread
{
    /**
     * 日志文件路径+文件名（不含文件扩展名）
     */
    private final String _curLogFile;
    
    /**
     * 日志文件的最大长度（单位：字节）
     */
    private final int _logFileMaxsize;
    
    /**
     * 日志文件的保存时间（单位：天）
     */
    private final int _logFileSaveTime;
    
    /**
     * 日志扩展名
     */
    private final String _log_extension = ".log";
    
    public MonitorLogFileStorage(String curLogFile, int logFileMaxSize, int logFileSaveTime)
    {
        this._curLogFile = curLogFile;
        this._logFileMaxsize = logFileMaxSize;
        this._logFileSaveTime = logFileSaveTime;
    }
    
    @Override
    public void run()
    {
        while (true)
        {
            try
            {
                File file = new File(this._curLogFile);
                File[] arr = new File(new File(file.getAbsolutePath()).getParent()).listFiles();
                if (arr != null)
                {
                    for (File f : arr)
                    {
                        // 先压缩
                        if (f.getName().endsWith(this._log_extension) && f.length() > this._logFileMaxsize)
                        {
                            String newFileName = f.getAbsolutePath().substring(0,
                                f.getAbsolutePath().lastIndexOf(this._log_extension))
                                + ".zip";
                            // System.out.println(f.getAbsolutePath() + "\n" + newFileName + "\n");
                            ZipFileUtil.zipFile(f.getAbsolutePath(), newFileName);
                            f.delete();
                        }
                        
                        Calendar cc = Calendar.getInstance();
                        cc.add(Calendar.DATE, -1 * this._logFileSaveTime);
                        Date time = cc.getTime();
                        Date fd = new Date(f.lastModified());
                        // 再将过期的文件删除
                        if (fd.before(time)) // fd.after(time)
                        {
                            if (f.getName().endsWith(".zip")
                                || (f.getName().endsWith(this._log_extension) && f.length() > this._logFileMaxsize))
                            {
                                f.delete();
                            }
                        }
                    }
                }
                
                Thread.sleep(1800000); // 1000*60*60=3600000（一个小时)（轮询周期）
            }
            catch (InterruptedException e)
            {
                e.printStackTrace();
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
        }
    }
}
