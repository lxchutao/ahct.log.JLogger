/*
 * 文 件 名:  LogLevel.java
 * 版    权:  Technologies Co., Ltd. Copyright YYYY-YYYY,  All rights reserved
 * 描    述:  <描述>
 * 修 改 人:  Administrator
 * 修改时间:  2013-6-15
 * 跟踪单号:  <跟踪单号>
 * 修改单号:  <修改单号>
 * 修改内容:  <修改内容>
 */
package ahct.log;

/**
 * 日志级别 从低到高依次顺序：DEBUG|INFO|WARN|ERROR|FATAL
 * 
 * @author ChuTao
 * @version [版本号, 2013-6-15]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 * Copyright © lxchutao(楚涛) 2011-2015  lxchutao@163.com  QQ:290363711  http://blog.csdn.net/chutao  http://lxchutao.cnblogs.com
 */
public enum LogLevel
{
    DEBUG, INFO, WARN, ERROR, FATAL
}

/**
 * 日志级别与字符串的相互转换
 * 
 * @author ChuTao
 * @version [版本号, 2013-6-16]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
class LogLevelHelper
{
    /**
     * 日志级别字符串转换为日志级别
     * 
     * @param strLogLevel 日志级别字符串
     * @return 日志级别，默认为DEBUG
     * @see [类、类#方法、类#成员]
     */
    static LogLevel StrToLogLevel(String strLogLevel)
    {
        LogLevel logLevel = LogLevel.DEBUG;
        if (strLogLevel != null)
        {
            String level = strLogLevel.toUpperCase();
            if (level.equals("DEBUG") || level.equals("INFO") || level.equals("WARN") || level.equals("ERROR")
                || level.equals("FATAL"))
            {
                logLevel = LogLevel.valueOf(level);
            }
        }
        return logLevel;
    }
}